package pacoteSBT4;

import estruturaDeDados.Grafo;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Wll
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        /* ====================================
                   ATRIBUTOS DA CLASSE
           ==================================== */
        
        int opcao; // Opção para trabalhar o menu principal.
        
        String tituloDasTelas = "Trabalho 4";
        
        /* ====================================
             ATRIBUTOS DE INSTÂNCIA DA CLASSE
           ==================================== */
        Grafo G = new Grafo();
        Operacoes O = new Operacoes();
        
        /* ====================================
                   OPERAÇÕES DA CLASSE
           ==================================== */
        
        do{
            
            opcao = menuPrincipal();
            
            switch(opcao){
                
                case 0: O.sair();
                        JOptionPane.showMessageDialog(null,"Tchau...",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        break;
                
                case 1: G.criarMA();
                        JOptionPane.showMessageDialog(null, "Arquivos lidos com sucesso!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        break;
                
                case 3: O.relatorioEmArquivo();
                        JOptionPane.showMessageDialog(null, "Relatório gerado com sucesso!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        break;
                
                default: JOptionPane.showMessageDialog(null,"Opção inválida!","Advertência",JOptionPane.WARNING_MESSAGE);
                
            }
            
        }while(opcao != 0);
        
    }
    
    // Método para acessar o menu principal da aplicação.
    private static int menuPrincipal(){
        
        return Integer.parseInt(JOptionPane.showInputDialog(null, 
                                                                  "========================= \n"
                                                                + " 1 - Ler arquivo texto    \n"
                                                                + " 2 - Gerar relatório      \n"
                                                                + " 0 - Sair da aplicação    \n"
                                                                + "========================= \n"
                                                                + " Digite uma opção válida  \n"
                                                                + "========================= \n",
                                                                     
                                                                "Menu principal", JOptionPane.QUESTION_MESSAGE));
        
    }
        
}