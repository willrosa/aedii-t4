package pacoteSBT4;

import estruturaDeDados.Grafo;
import java.io.IOException;

/**
 *
 * @author Wll
 */
public class Operacoes {

    /* ====================================
                ATRIBUTOS DA CLASSE
       ==================================== */
    
    private final String arquivoAdjacencia = ".\\src\\arquivos\\adjacencia.txt";
    private final String arquivoDistancias = ".\\src\\arquivos\\distancias.txt";
    private final String arquivoSaida      = ".\\src\\arquivos\\saida.txt";
    
    String tituloDasTelas = "Grafo";

    private String[] dadosDoArquivoA; // Arquivo de adjacencia
    private String[] dadosDoArquivoD; // Arquivo de distâncias
    
    /* ====================================
         ATRIBUTOS DE INSTÂNCIA DA CLASSE
       ==================================== */

    // Atributo para manipular o grafo G.
    private Grafo G = new Grafo();

    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */

    public Grafo getG() {
        return this.G;
    }

    private void setG(Grafo _g) {
        this.G = _g;
    }

    /* ====================================
               OPERAÇÕES DA CLASSE
       ==================================== */
    
    // Método para criar registrar uma nova instância do objeto grafo.
    private void criarGrafo(){
        
        this.setG(new Grafo());
        
    }
    
    // Método que implementa o método para ler os arquivos de entrada de dados.
    public void lerArquivos() throws IOException{
        
        dadosDoArquivoA = Arquivo.lerArquivo(this.arquivoAdjacencia);
        dadosDoArquivoD = Arquivo.lerArquivo(this.arquivoDistancias);
        
        
        
    }
    
    // Método que implementa o método para gerar o relatório de saída de dados.
    public void relatorioEmArquivo(){
        
        String texto = null;
        
        Arquivo.gerarRelatorio(this.arquivoSaida,texto);
        
        
        
    }
    
    // Método para registrar null ao objeto G.
    public void sair(){
        
        this.setG(null);
        
    }
    
}
