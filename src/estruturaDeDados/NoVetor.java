package estruturaDeDados;

/**
 *
 * @author Wll
 */
public class NoVetor {
    
    /* ====================================
                ATRIBUTOS DA CLASSE
       ==================================== */
    
    // Descritor da estrutura do vetor dinâmico.
    private char vertice;
    
    private No primeiroNo;
    private int quantidadeDeNos;
    private No ultimoNo;
    
    /* ====================================
            MÉTODO CONSTRUTOR DA CLASSE
       ==================================== */
    public NoVetor() { }

    /* ====================================
                 MÉTODO da CLASSE
       ==================================== */
    public void iniciarPosicaoVetor(){
        this.setPrimeiroNo(null);
        this.setQuantidadeDeNos(0);
        this.setUltimoNo(null);
    }
    
    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */

    public char getVertice() {
        return this.vertice;
    }

    public void setVertice(char _v) {
        this.vertice = _v;
    }
    
    public No getPrimeiroNo() {
        return this.primeiroNo;
    }

    public void setPrimeiroNo(No _pN) {
        this.primeiroNo = _pN;
    }

    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }

    public void setQuantidadeDeNos(int _qN) {
        this.quantidadeDeNos = _qN;
    }

    public No getUltimoNo() {
        return this.ultimoNo;
    }

    public void setUltimoNo(No _uN) {
        this.ultimoNo = _uN;
    }
    
}

