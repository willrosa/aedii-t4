package estruturaDeDados;

/**
 *
 * @author Wll
 */
public class No {
    
    /* ====================================
                ATRIBUTOS DA CLASSE
       ==================================== */
    
    private String vertice;
    private int distancia;
    
    private No proximo;
    
    /* ====================================
            MÉTODO CONSTRUTOR DA CLASSE
       ==================================== */
    
    public No() {
    
        this.setProximo(null);
        
    }
    
    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */
    
    public void setValor(String _v){
        this.vertice = _v;
    }
    
    public String getValor(){
        return this.vertice;
    }
    
    public void setProximo(No _referencia){
        this.proximo = _referencia;
    }
    
    public No getProximo(){
        return this.proximo;
    }
    
    public void setDistancia(int _distancia){
        this.distancia = _distancia;
    }
    
    public int getDistancia(){
        return this.distancia;
    }
    
}

