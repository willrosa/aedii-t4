
package estruturaDeDados;

import java.io.IOException;
import javax.swing.JOptionPane;
import pacoteSBT4.Arquivo;

/**
 *
 * @author Wll
 */
public class Grafo {

    /* ====================================
                ATRIBUTOS DA CLASSE
       ==================================== */
    
    private final String arquivoMA = ".\\src\\arquivos\\adjacencia.txt";
    private final String arquivoMD = ".\\src\\arquivos\\distancias.txt";
    
    String tituloDasTelas = "Grafo";

    private String dadosDoArquivo[];
    
    private int MatrizAdjacencia[][];
    private int MatrizDistancias[][];
    
    private int qtdVertices;
    
    private NoVetor[] listaDeAdjacencia;
    
    private String[] Vertices;
    
    /* ====================================
            MÉTODO CONSTRUTOR DA CLASSE
       ==================================== */
    
    public Grafo(){
    
        this.setQtdVertices(0);
    
    }
    
    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */

    public String[] getDadosDoArquivo() {
        return this.dadosDoArquivo;
    }

    public void setDadosDoArquivo(String[] _dDA) {
        this.dadosDoArquivo = _dDA;
    }
    
    public int[][] getMatrizDeAdjacencia() {
        return this.MatrizAdjacencia;
    }

    public void setMatrizDeAdjacencia(int[][] _mA) {
        this.MatrizAdjacencia = _mA;
    }

    public int[][] getMatrizDeDistancias() {
        return this.MatrizDistancias;
    }

    public void setMatrizDeDistancias(int[][] _mD) {
        this.MatrizDistancias = _mD;
    }
    
    public int getQtdVertices() {
        return this.qtdVertices;
    }

    public void setQtdVertices(int _qV) {
        this.qtdVertices = _qV;
    }

    public NoVetor[] getListaDeAdjacencia() {
        return this.listaDeAdjacencia;
    }

    public void setListaDeAdjacencia(NoVetor[] _lA) {
        this.listaDeAdjacencia = _lA;
    }
    
    /* ====================================
               OPERAÇÕES DA CLASSE
       ==================================== */
    
    // Método para ler o arquivo de entrada de dados.
    private void lerArquivo(String _arquivo) throws IOException{
        
        this.dadosDoArquivo = Arquivo.lerArquivo(_arquivo);
                
    }
    
    // Método para criar um arquivo do tipo texto.
    private void criarArquivo(){
        
        
        
    }
    
    // Método para criar a matriz de adjacência.
    public void criarMA() throws IOException{
        
        this.lerArquivo(this.arquivoMA);
        
        //this.setQtdVertices(Integer.parseInt(String.valueOf(this.getDadosDoArquivo()[0].charAt(0))));
        this.setQtdVertices(Integer.parseInt(String.valueOf(this.getDadosDoArquivo().length)));
        
        JOptionPane.showMessageDialog(null, "Quantidade de Vértices: " + this.qtdVertices);
        
        this.setMatrizDeAdjacencia(new int[this.getQtdVertices()][this.getQtdVertices()]);
        
        int p = 1;
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            for (int C = 0; C < this.getQtdVertices(); C++){
                
                //this.getMatrizDeAdjacencia()[L][C] = Integer.parseInt(this.getDadosDoArquivo()[p]);
                p++;
            
            }
        
        }
        JOptionPane.showMessageDialog(null, this.getMatrizDeAdjacencia()[0][0]);
        int arestas = 0;
//        for(int i = 0; i < this.qtdVertices; i ++){
//            for(int j = 0; j < this.qtdVertices; j++){
//                if(i == 0){
//                    
//                        
//                    }
//                }
//            }            
//        }       
    }
    
    // Método para resgatar o valor de um determinado quadrante da matriz.
    // _VO -> Vértice origem
    // _VD -> Vértice destino
    private int resgatarQuadrante(int _VO, int _VD, int opcao){
        
        return opcao == 1 ? this.getMatrizDeAdjacencia()[_VO][_VD] : this.getMatrizDeDistancias()[_VO][_VD];
        
    }
    
    // Método para imprimir a matriz de adjacência.
    public void imprimirMA(){
        
        String texto = "";
        
        texto += "Matriz de adjacência do Grafo G:\n\n";
        texto += "   ";
        
        for (int L = 0; L < this.getQtdVertices(); L++)
            texto += L + " ";
        
        texto += "\n";
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            texto += L + " ";
            
            for (int C = 0; C < this.getQtdVertices(); C++)
                texto += this.resgatarQuadrante(L,C,1) + " ";
            
            texto += "\n";
            
        }
        
        JOptionPane.showMessageDialog(null, texto);
        
    }
    
    // Método para imprimir a matriz de adjacência.
    public void imprimirMD(){
        
        String texto = "";
        
        texto += "Matriz de distâncias do Grafo G:\n\n";
        texto += "   ";
        
        for (int L = 0; L < this.getQtdVertices(); L++)
            texto += L + " ";
        
        texto += "\n";
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            texto += L + " ";
            
            for (int C = 0; C < this.getQtdVertices(); C++)
                texto += this.resgatarQuadrante(L,C,2) + " ";
            
            texto += "\n";
            
        }
        
        JOptionPane.showMessageDialog(null, texto);
        
    }
    
    private void inicializarLA() throws IOException{
        
        // Verificando se já foi criada a matriz de adjacência.     
        if(this.getQtdVertices() > 0){
            
            // Lendo o arquivo matriz de distâncas.
            this.lerArquivo(this.arquivoMD);

            // Instanciando a matriz de distâncias
            this.setMatrizDeDistancias(new int[this.getQtdVertices()][this.getQtdVertices()]);
        
            // Criando uma instância do vetor.
            this.setListaDeAdjacencia(new NoVetor[this.getQtdVertices()]);
            
            // Começando a contagem a partir da quantidade de vértices do grafo.
            int p = this.getQtdVertices()+1;

            // Preenchendo a matriz de distâncias
            for (int L = 0; L < this.getQtdVertices(); L++){

                for (int C = 0; C < this.getQtdVertices(); C++){

                   // this.getMatrizDeDistancias()[L][C] = Integer.parseInt(String.valueOf(this.getDadosDoArquivo().charAt(p)));
                    p++;

                }

            }
            
            this.Vertices = new String[this.getQtdVertices()];
            
            // Inicializando as listas encadeadas para cada posição do vetor
            for(int i = 0; i < this.getQtdVertices(); i++){
            
                //this.Vertices[i] = String.valueOf(this.getDadosDoArquivo().charAt(i+1));
            
                this.getListaDeAdjacencia()[i] = new NoVetor();
                
                this.getListaDeAdjacencia()[i].iniciarPosicaoVetor();
                //this.getListaDeAdjacencia()[i].setVertice(this.getDadosDoArquivo().charAt(i+1));
                
            }
            
        }
        
    }
    
    // Método para criar a lista de adjacência.
    public void criarLA() throws IOException{
        
        this.inicializarLA();
        
        for (int L = 0; L < this.getQtdVertices(); L++){

            for (int C = 0; C < this.getQtdVertices(); C++){
                
                if (this.getMatrizDeDistancias()[L][C] != 0){
                    
                    No novoNo = new No();
                    
                    novoNo.setValor(this.Vertices[C]);
                    novoNo.setDistancia(this.getMatrizDeDistancias()[L][C]);
                    
                    if(this.getListaDeAdjacencia()[L].getQuantidadeDeNos() == 0){
                        
                        this.getListaDeAdjacencia()[L].setPrimeiroNo(novoNo);
                        
                    }else{
                        
                        this.getListaDeAdjacencia()[L].getUltimoNo().setProximo(novoNo);
                        
                    }
                    
                    this.getListaDeAdjacencia()[L].setUltimoNo(novoNo);
                    this.getListaDeAdjacencia()[L].setQuantidadeDeNos(this.getListaDeAdjacencia()[L].getQuantidadeDeNos()+1);
                    
                }
                
            }

        }
        
    }
    
    // Método para imprimir a lista de adjacência.
    public void imprimirLA(){
        
        String texto = "";
        No aux;
        
        texto += "Lista de adjacência do Gráfo G:\n\n";
        
        for (int i = 0; i < this.getQtdVertices(); i++){
            
            texto += this.getListaDeAdjacencia()[i].getVertice() + " : ";
            
            aux = this.getListaDeAdjacencia()[i].getPrimeiroNo();
            
            while(aux != null){
                
                texto += aux.getValor() + " -> ";
                aux = aux.getProximo();
                
            }
            
            texto += "null\n";
            
        }
        
        JOptionPane.showMessageDialog(null, texto);
        
    }
    
    // Método para registrar null ao objeto G.
    public void sair(){
        
        // Inicializando as listas encadeadas para cada posição do vetor
        for(int i = 0; i < this.getQtdVertices(); i++){

            this.getListaDeAdjacencia()[i] = null;

            this.getListaDeAdjacencia()[i].iniciarPosicaoVetor();
            //this.getListaDeAdjacencia()[i].setVertice(this.getDadosDoArquivo().charAt(i+1));

        }
        
    }
    
}
